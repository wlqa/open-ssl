### 浏览器身份验证
> 本实验向您展示 Web 浏览器的权限是如何工作的。
#### 1. 生成服务器证书
> 我们将创建密钥类型为rsa:4096的CA和服务器证书。详细的源代码已经推送到github。有帮助的时候请在上面加星号。CA和服务器证书将在运行gen.sh后生成
>  1. 生成服务器CA的私钥和自签名证书
```
[root@VM-88-3-centos openssl]# openssl  req -x509 -newkey rsa:4096 -days 365 -nodes -keyout ca.key -out ca.cert -subj "/CN=localhost/emailAddress=ca@mail.com"
Generating a 4096 bit RSA private key
...........................................++
.........++
writing new private key to 'ca.key'
-----

```
> 查看CA证书详细信息

```
[root@VM-88-3-centos openssl]# openssl x509 -in ca.cert -noout -text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            e4:b0:1e:41:61:fe:50:99
    Signature Algorithm: sha256WithRSAEncryption                    #签名算法
        Issuer: CN=localhost/emailAddress=ca@mail.com                #发行机构
        Validity
            Not Before: Mar 13 12:33:17 2022 GMT
            Not After : Mar 13 12:33:17 2023 GMT
        Subject: CN=localhost/emailAddress=ca@mail.com
        Subject Public Key Info:                                        #证书持有者（CA）公开信息
            Public Key Algorithm: rsaEncryption                          #公钥信息
                Public-Key: (4096 bit)
                Modulus:
                    00:b2:9b:14:09:17:80:69:df:ed:55:84:0e:71:21:
                    7f:01:c1
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier:                             #主体秘钥标识符
                7E:2F:6B:BC:49:00:94:27:8B:1A:3E:40:14:64:B9:F5:C8:59:21:1C
            X509v3 Authority Key Identifier:                         #秘钥标识符
                keyid:7E:2F:6B:BC:49:00:94:27:8B:1A:3E:40:14:64:B9:F5:C8:59:21:1C

            X509v3 Basic Constraints: 
                CA:TRUE        
    Signature Algorithm: sha256WithRSAEncryption
         31:6d:b2:4d:62:85:3b:fc:5e:38:cf:4c:e1:7f:33:ae:a8:03:

```
> 2. 生成web服务器的私钥和证书签名请求(CSR)

```
[root@VM-88-3-centos openssl]# openssl req -newkey rsa:4096 -nodes -keyout server.key -out server.req -subj "/CN=localhost/emailAddress=server@mail.com"
Generating a 4096 bit RSA private key
.......................................................................++
......++
writing new private key to 'server.key'
-----
[root@VM-88-3-centos openssl]# 
[root@VM-88-3-centos openssl]# ls
bak  ca.cert  ca.key  gen.sh  server.ext  server.key  server.req
```

> 3. 使用CA的私钥对web服务器的CSR进行签名，并获得已签名的证书
```
[root@VM-88-3-centos openssl]# openssl x509 -req -in server.req -days 60 -CA ca.cert -CAkey ca.key -CAcreateserial -out server.cert -extfile server.ext
Signature ok
subject=/CN=localhost/emailAddress=server@mail.com
Getting CA Private Key
[root@VM-88-3-centos openssl]# ls
bak  ca.cert  ca.key  ca.srl  gen.sh  server.cert  server.ext  server.key  server.req
#查看server的证书
openssl x509 -in server.cert -noout -text
```
> 4. 通过根CA验证服务器证书

```
[root@VM-88-3-centos openssl]# openssl verify -CAfile ca.cert server.cert 
server.cert: OK

```
#### 4. 创建一个网页

```
[root@VM-88-3-centos openssl]# cat index.html 
hello word!
```


#### 5. 启动openssl服务器    ###index.html 放在哪里测试
> 要创建一个简单的服务器，我们可以使用openssl s_server命令，它实现了一个通用的SSL/TLS服务器，该服务器使用SSL/TLS侦听给定端口上的连接。请发出以下命令启动TLS服务器。

```
[root@VM-88-3-centos openssl]# openssl s_server -accept 20000 -cert server.cert -key server.key  -WWW -debug -tlsextdebug
Using default temp DH parameters
ACCEPT

#保持不断，然后访问 可以看到HTTPS页面，index.html 也在当前路径下
```
> 这个命令中的有用选项:
> accept:可选的TCP主机和端口，用来监听连接。如果不指定，则使用*:4433。
> 证书和密钥:服务器端证书和密钥由gen.sh生成
> 模拟一个简单的web服务器。它非常有用。这些页面将相对于当前目录进行解析，例如，如果请求的URL是https://localhost:20000/index.html，那么文件。/index.html将被加载。
> Debug和tlsextdebug:显示所有流量的详细转储信息。
  
#### 6. 发送请求
> 服务器启动成功后，我们可以使用浏览器发送如下请求。
```
https://localhost:20000/index.html
```
![输入图片说明](https-test.png)
>但会出现安全风险警告。因为它的证书颁发者是未知的，并且由我们自己创建的服务器证书不受浏览器的信任。firefox-security-warning








